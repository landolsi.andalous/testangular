import { Injectable, ɵConsole } from '@angular/core';
import decode from 'jwt-decode';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
@Injectable()
export class AuthentificationService {

  private url= 'http://localhost:3000/auth'

  constructor(private router: Router, private httpClient: HttpClient) { }

  login(credentials: { email: string, password: string }) {
    return this.httpClient.post(`${this.url}/login`, credentials);
  }

  register(user: any) {
    return this.httpClient.post(`${this.url}/register`, user);

  }


  decode(token) {
    if (token) {
      return decode(token);
    } else {
      return null;
    }
  }

  isValidToken(token) {
    const expDate = decode(token).exp;
    const now = new Date().getTime();
    if (expDate < now) {
      return false
    } else return true;
  }

  logout() {
    localStorage.removeItem('token');
  }

  changePwd(data: { oldPassword: string, newPassword: string }) {
    return this.httpClient.post( `${this.url}/changepassword`, data);
  }

  getToken() {
    return of (localStorage.getItem('token')); 
  }

}