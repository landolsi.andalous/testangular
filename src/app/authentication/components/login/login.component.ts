import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthentificationService } from 'src/app/authentication/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public hide = false;
  public loginFormGroup: FormGroup;
  public credentials: any;
  public isWrongCredentials = "";

  constructor(
    private router: Router,
    private authService: AuthentificationService
  ) { }

  ngOnInit() {
    this.buildForm();

  }

  buildForm() {
    this.loginFormGroup = new FormGroup({
      'email': new FormControl('xxx@gmail.com'),
      'password': new FormControl('xxxx')
    });
    this.loginFormGroup.valueChanges.subscribe(
      (credentials: any) => this.credentials = credentials
    );
  }

  login() {
    if (this.credentials.email === "xxx@gmail.com" && this.credentials.password === 'xxxx') {
      localStorage.setItem('token', 'ahagjhkajhkjla');
      this.router.navigate(['/tasks']);


    }
    // this.authService.login(this.credentials).subscribe(
    //   (data: any) => {
    //     console.log('login');
    //     localStorage.setItem('token', data.token);
    //     console.log('redirect');
    //     this.router.navigate(['/tasks']);
    //   },
    //   (err: HttpErrorResponse) => {
    //     if (err.status == 400) {
    //       console.log(err.error.err)
    //       this.isWrongCredentials = err.error.err;
    //     }
    //   }
    // )
  }





}