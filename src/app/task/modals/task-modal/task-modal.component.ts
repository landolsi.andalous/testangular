import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Task } from '../task';

@Component({
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.css']
})
export class TaskModalComponent implements OnInit {

  public taskForm: FormGroup;

  constructor(public dialogRef: MatDialogRef<TaskModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.taskForm = new FormGroup({
      'id': new FormControl( { value: this.data ? this.data.data.task.id : '', disabled: this.data ? true: false} ),
      'name': new FormControl(  this.data ? this.data.data.task.name : '' ),
    });
 
  }

  addTask() {
    const id = this.taskForm.get('id').value;
    const name = this.taskForm.get('name').value;
    const task = {
      id: id,
      name: name
    }
    this.dialogRef.close(task);
  }

  editTask() {
    const id = this.taskForm.get('id').value;
    const name = this.taskForm.get('name').value;
    const task = {
      id: id,
      name: name
    }
    this.dialogRef.close(task);
  }
}
