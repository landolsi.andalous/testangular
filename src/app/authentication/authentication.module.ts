import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatCheckboxModule, MatCardModule, MatIconModule, MatInputModule, MatSelectModule, MatDialog, MatDialogModule, MatSnackBar, MatSnackBarModule } from '@angular/material';


import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';
import { LoginComponent } from './components/login/login.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthentificationService } from './services/auth.service';
import { ToastModule } from '../shared/components/toast/toast.module';
import { AuthGuard } from '../shared/guards/auth.guard';

const materialModule = [
  MatButtonModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatDialogModule,
  MatSnackBarModule
]

@NgModule({
  declarations: [AuthenticationComponent, LoginComponent, ForgetPasswordComponent, ResetPasswordComponent, RegisterComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ...materialModule,
    ToastModule
  ],
  providers: [
    AuthentificationService,
    MatDialog,
    AuthGuard
  ],
  entryComponents : [
  ]

})
export class AuthenticationModule { }
