import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app-test';

  constructor(private swUpdate: SwUpdate) {
    this.reloadCache();
  }

  reloadCache() {
    if(this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(
        () =>
        {
          if(confirm('New version avaible')) {
            window.location.reload();
          }
        }
      )
    }
  }
 

}
