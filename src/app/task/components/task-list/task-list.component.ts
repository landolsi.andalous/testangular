import { Component, EventEmitter, Input, ViewChild, ChangeDetectionStrategy, OnChanges, SimpleChanges, Output } from '@angular/core';
import {MatPaginator} from '@angular/material';
 

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnChanges {
  
  @Input()  dataSource;
  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @Output() editTaskEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteTaskEvent: EventEmitter<any> = new EventEmitter<any>();

  displayedColumns: string[] = ['id', 'name', 'actions'];

  ngOnChanges(changes: SimpleChanges): void {
      if (changes['dataSource'].currentValue != undefined) {
        this.dataSource.paginator = this.paginator;
    }
  }

  editTask(i, row) {
    this.editTaskEvent.emit({ index: i, task: row});
  }

  deleteTask(i, row) {
    this.deleteTaskEvent.emit({ index: i, task: row})
  }
}
