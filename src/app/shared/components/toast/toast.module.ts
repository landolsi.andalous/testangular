import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from './toast.component';
import { ToastService } from './toast.service';
import { MatSnackBar } from '@angular/material';



@NgModule({
  declarations: [ToastComponent],
  imports: [
    CommonModule
  ],
  entryComponents:[
    ToastComponent
  ],
  providers: [
    ToastService,
    MatSnackBar
  ],

})
export class ToastModule { }
