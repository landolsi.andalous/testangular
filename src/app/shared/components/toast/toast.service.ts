import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ToastComponent } from './toast.component';

@Injectable()
export class ToastService {

  constructor(public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  openSnackBar(description: string, error?: boolean) {
    this.snackBar.openFromComponent(ToastComponent, {
      verticalPosition: 'bottom' ,
      horizontalPosition: 'right' ,
      duration : 5000,
      panelClass : error ? 'toast-error' : 'toast-success',
      data : {
        description : description
      }
    });

  }
}
