import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/authentification/login',
        pathMatch: 'full'
    },
    {
        path: 'authentification',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
    {
        path: 'tasks',
        loadChildren: './task/task.module#TaskModule',
        canActivate:[AuthGuard]
    },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppoutingModule { }
