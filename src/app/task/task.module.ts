import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskRoutingModule } from './task-routing.module';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskService } from './services/task.service';
import { TaskComponent } from './task.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule, MatPaginatorModule, MatToolbarModule, MatIconModule, MatDialog, MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { TaskModalComponent } from './modals/task-modal/task-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TaskRoutingModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [TaskListComponent, TaskComponent, TaskModalComponent ],
  providers: [TaskService, MatDialog],
  entryComponents:[
    TaskModalComponent
  ]
})
export class TaskModule { }
