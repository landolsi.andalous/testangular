import { Component, OnInit } from '@angular/core';
import { TaskService } from './services/task.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { TaskModalComponent } from './modals/task-modal/task-modal.component';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  dataSource: MatTableDataSource<any>;

  public tasks: any[];

  constructor(private taskService: TaskService, public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getAllTasks();
  }

  getAllTasks() {
    console.log('test');
    this.taskService.getAllTasks().subscribe(data => {
      this.dataSource = new MatTableDataSource<any>(data);
      console.log(this.dataSource );
    });
  }

  /*editTask(data) {
    const dialogRef = this.dialog.open(TaskModalComponent, {
      width: '25%',
      data: { data: data }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.taskService.updateTask(result).subscribe(
          () => {
            this.getAllTasks();
          }
        )
        }
    });
  }

  addTask() {
    const dialogRef = this.dialog.open(TaskModalComponent, {
      width: '25%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.taskService.addTask(result).subscribe(
          () => {
            this.getAllTasks();
          }
        )
      }
    });
  }

  deleteTask(data: any) {
   
        this.taskService.deleteTask(data.task.id).subscribe(
          () => {
            this.getAllTasks();
          
          }
        )
      }*/
}
