import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../modals/task';
import { Observable } from 'rxjs';

@Injectable()
export class TaskService {

  private url = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) { }

  getAllTasks(): Observable<Task[]> {
    return this.httpClient.get<Task[]>(`${this.url}/tasks`);
  }

  addTask(task: Task): Observable<Task> {
    return this.httpClient.post<Task>(`${this.url}/tasks`, task);
  }

  deleteTask(idTask: number) {
    return this.httpClient.delete(`${this.url}/tasks/${idTask}`);
  }

  updateTask(task: Task): Observable<Task> {
    return this.httpClient.put<Task>(`${this.url}/tasks/${task.id}`, task);
  }
}
