import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationModalComponent } from './confirmation-modal.component';
import { MatDialogModule, MatButtonModule } from '@angular/material';

const materialDep = [
  MatDialogModule,
  MatButtonModule
]

@NgModule({
  declarations: [ConfirmationModalComponent],
  imports: [
    CommonModule,
    ...materialDep
    
  ],
  entryComponents: [
    ConfirmationModalComponent
  ]
})
export class ConfirmationModule { }
