import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { strengthPwdValidator, MatchPassword } from 'src/app/shared/validators/validtors';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  public resetPwdForm: FormGroup;
  public token: string;
  public hide1 =false;
  public hide =false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
  ) {} 

  ngOnInit() {
    let url: string = this.router.url.substring(0, this.router.url.indexOf("?"));
    this.token = this.router.url.substring(this.router.url.indexOf("=") + 1, this.router.url.length);
    this.initForm();
  }

  initForm() {
    this.resetPwdForm = this.fb.group({
      password: ['', [strengthPwdValidator, Validators.required, Validators.minLength(8)]],
      confirmPassword: ['']
    }, {
        validator: MatchPassword
      });
  }

  resetPassword() {
    const data = {
      password : this.resetPwdForm.get('password').value,
      token: this.token
    }
    // this.authService.resetPasword(data).subscribe(
    //   () => this.router.navigate(['/authentification/login']),
    //   (err: HttpErrorResponse) => {
    //   }
    // );

    
  }
}
