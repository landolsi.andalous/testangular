import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificationService } from 'src/app/authentication/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private _router: Router,
    private authService: AuthentificationService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      console.log('guards')
      const token = localStorage.getItem('token');
      // console.log(token);
      // console.log(this.authService.isValidToken(token))
    if (token ) {
      return true;
    } else {
      this._router.navigate(['/authentification/login']);
      return false;
    }
  }
}
