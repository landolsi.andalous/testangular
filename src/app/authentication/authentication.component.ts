import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  
  public selectedLanguage = null;
  public isLgChanged = false;


  constructor(

    private router: Router
  ) { }

  ngOnInit() {
  
  }

  changeLanguage(language: string) {

  }
}
