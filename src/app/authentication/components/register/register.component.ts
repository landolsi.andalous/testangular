import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthentificationService } from 'src/app/authentication/services/auth.service';
import { ToastService } from 'src/app/shared/components/toast/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public registerFormGroup: FormGroup;

  constructor(private router: Router, private authService: AuthentificationService, private toastService: ToastService) { }
  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.registerFormGroup = new FormGroup({
      'firstName': new FormControl(),
      'lastName': new FormControl(),
      'email': new FormControl(),
      'password': new FormControl()
    });
    
  }

  addUser() {
    console.log(this.registerFormGroup.value);
    this.authService.register(this.registerFormGroup.value).subscribe(
      data => {
        this.toastService.openSnackBar('Success')
        this.router.navigate(['/authentification/login'])
            },
      error => console.log(error)
    );
  }

}
