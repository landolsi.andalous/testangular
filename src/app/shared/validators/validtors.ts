import { AbstractControl } from '@angular/forms';
declare var require: any;

export function strengthPwdValidator(control: AbstractControl): { [key: string]: boolean } | null {
    let tests = [/[0-9]/, /[a-z]/, /[A-Z]/, /[^A-Z-0-9]/i];
    let s = 0;
    for (let i = 0; i < tests.length; i++) {
        if (tests[i].test(control.value)) {
            s++;
        }
    }
    if (control.value !== undefined && (isNaN(control.value)) && s < 3) {
        return { 'isStrength': true };
    }
    return null;
}



export function MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value;
    let verifyPassword = AC.get('confirmPassword').value;
    if (password != verifyPassword) {
        AC.get('confirmPassword').setErrors({ MatchPassword: true })
    } else {
        return null
    }
}

export function validEmail(control: AbstractControl) {
    const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (control.value == null)
        return null;

    if (!emailPattern.test(control.value)) {
        return { "pattern": true };
    }
    return null;
}

export function validFDate(control: AbstractControl) {
    const datePattern = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
    if (control.value == null)
        return null;

    if (!datePattern.test(control.value)) {
        return { "isValidDate": true };
    }
    return null;
}