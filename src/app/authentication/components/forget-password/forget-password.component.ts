import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  public initPwdForm: FormGroup;
  public showError = false;

  constructor(
    private router: Router,
    private dialog: MatDialog  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.initPwdForm = new FormGroup({
      'accountNumber': new FormControl('')
    })
  }

  initPassword() {
    const accountNumber = this.initPwdForm.get('accountNumber').value;
    // this.authService.getMailByAccountNumber(accountNumber).subscribe(
    //   (data: any) => {
    //     const dialog = this.dialog.open(null, {
    //       data: {
    //         body: this.translate.instant('sendMailMsg') + ` "${data.email}"`,
    //         title: this.translate.instant('sendMailTitle'),
    //         btnName: 'OK',
    //         theme: 'primary'
    //       }
    //     });
    //     dialog.afterClosed().subscribe(result => {
    //       this.router.navigate(['/authentification/login'])
    //     });
    //   },
    //   (err: any) => {
    //     if (err.status == 404) {
    //       this.showError = true;
    //     }
    //   }
    // )
  }
}
